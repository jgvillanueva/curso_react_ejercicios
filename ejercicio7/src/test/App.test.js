import React from "react";
import {configure, shallow} from "enzyme";
import chai, {expect} from "chai";
import chaiEnzyme from "chai-enzyme";
import Adapter from "enzyme-adapter-react-16";
import App from "../App";

import { act } from "react-dom/test-utils";
import Botonera from "../Botonera/Botonera";
import Caja from "../Caja/Caja";

configure({
    adapter: new Adapter()
});

let wrapper;
beforeEach(() => {
    act(() => {
        wrapper = shallow(<App />);
    });
});
afterEach(() => {
    wrapper.unmount();
});


describe("Testing App Component", () => {
    describe("Testing h1 title", () => {
        it("App renders a h1 title", () => {
            const message = <h1>React-redux</h1>;
            expect(wrapper).to.contain(message);
        });
        chai.use(chaiEnzyme());
    });

    describe("App render Botonera component", () => {
        it("App renders Botonera", () => {
            const botonera = <Botonera/>;
            expect(wrapper).to.contain(botonera);
        });
        chai.use(chaiEnzyme());
    });

    describe("App render Caja component", () => {
        it("App renders Caja", () => {
            const caja = <Caja/>;
            expect(wrapper).to.contain(caja);
        });
        chai.use(chaiEnzyme());
    });

    describe("Botonera functionality", () => {
        let botonera;
        let caja;
        beforeEach(() => {
            act(() => {
               botonera = wrapper.find('Botonera');
               caja = wrapper.find('Caja');
            });
        });
        it("Clicks on red button", () => {
            const button = botonera.dive().first('li.nav-item');
            act(() => {
                button.simulate('click');
            });
            const card = caja.dive().find('.card');
            /*console.log('********');
            console.log(card.debug());
            console.log('********');*/
            expect(card).to.have.style('background-color', '#ff0000');
        });
        it("Clicks on green button", () => {
            const button = botonera.dive().find('ul').childAt(1).find('.nav-link');
            act(() => {
                button.simulate('click');
            });
            const card = caja.dive().find('.card');
            expect(card).to.have.style('background-color', '#00ff00');
        });
        chai.use(chaiEnzyme());
    });
});

