import React from "react";
import {configure, shallow} from "enzyme";
import chai, {expect} from "chai";
import chaiEnzyme from "chai-enzyme";
import Adapter from "enzyme-adapter-react-16";
import App from "../App";

import { act } from "react-dom/test-utils";
import Botonera from "../Botonera/Botonera";

configure({
    adapter: new Adapter()
});

let wrapper;
beforeEach(() => {
    act(() => {
        wrapper = shallow(<Botonera />);
    });
});
afterEach(() => {
    wrapper.unmount();
});


describe("Testing Botonera Component", () => {
    describe("Testing h1 title", () => {
        it("App renders a h1 title", () => {
            const liItems = wrapper.find('li.nav-item');
            expect(liItems.length).to.equal(3);
        });
        chai.use(chaiEnzyme());
    });
});

