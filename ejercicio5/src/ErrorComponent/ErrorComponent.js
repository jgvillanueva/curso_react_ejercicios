import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import PropTypes from 'prop-types';

class ErrorComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let error;
        if(this.props.errorText !== '' && this.props.touched) {
            error = <div className="my-3 alert alert-danger">
                {this.props.errorText}
            </div>
        }
        return (
            <div>
                {error}
            </div>
        )
    }
}
export default ErrorComponent;


// el valor es obligatorio y mayor de 10 caracteres
let validacionCustom = (props, propName, componentName) => {
    if(props[propName]){
        let value = props[propName];

        if(value < 10)
            return new Error(
                `${propName} en ${componentName} no debe tener menos de 10 caracteres`
            );

        return null;
    }
    return new Error(
        `${propName} en ${componentName} es obligatorio`
    );
}

ErrorComponent.propTypes = {
    //errorText: PropTypes.string.isRequired,
    errorText: validacionCustom,
    touched: PropTypes.bool.isRequired,
}
/*
ErrorComponent.defaultProps={
    errorText: "ErrorComponent desconocido",
    touched: true,
}
*/
