import React, { Component } from 'react';
import { useFormik } from 'formik';

const FormUncontrolled = () => {
    const validate = values => {
        const errors = {};
        if (!values.asunto) {
            errors.asunto = 'Required';
        } else if (values.asunto.length > 5) {
            errors.asunto = 'Must be 5 characters or less';
        }
        if (!values.mensaje) {
            errors.mensaje = 'Required';
        } else if (values.mensaje.length > 15) {
            errors.mensaje = 'Must be 15 characters or less';
        }
        return errors;
    };

    const formik = useFormik({
        initialValues: {
            asunto: '',
            mensaje: '',
        },
        validate,
        onSubmit: values => {
            console.log(JSON.stringify(values, null, 2));
        },
    });

    return (
        <div className='card'>
            <div className="card-header">
                <h1>Uncontrolled form</h1>
            </div>
            <div className="card-body">
                <form className='formulario'  onSubmit={formik.handleSubmit}>
                    <div className='form-group' >
                        <label htmlFor='asunto'>Asunto</label>
                        <input
                            type='text'
                            className='form-control'
                            name='asunto'
                            onChange={formik.handleChange}
                            value={formik.values.asunto}
                        />
                        {
                            formik.errors.asunto ?
                                <div className="my-3 alert alert-danger">{formik.errors.asunto}</div>
                                :
                                null
                        }
                    </div>
                    <div className='form-group' >
                        <label htmlFor='mensaje'>Mensaje</label>
                        <textarea
                            className='form-control'
                            name='mensaje'
                            onChange={formik.handleChange}
                            value={formik.values.mensaje}
                        ></textarea>
                        {
                            formik.errors.mensaje ?
                                <div className="my-3 alert alert-danger">{formik.errors.mensaje}</div>
                                :
                                null
                        }
                    </div>
                    <button disabled={!formik.isValid} className="btn btn-primary my-2" type="submit" >
                        Enviar
                    </button>
                </form>
            </div>
        </div>
    )

}
export default FormUncontrolled;


/*
import React, { Component } from 'react';
class FormUncontrolled extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);

        this.asuntoInput = React.createRef();
        this.mensajeInput = React.createRef();
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log(this.asuntoInput.current.value, this.mensajeInput.current.value);
    }

    render() {
        return (
            <div className='card'>
                <div className="card-header">
                    <h1>Uncontrolled form</h1>
                </div>
                <div className="card-body">
                    <form className='formulario'  onSubmit={this.handleSubmit}>
                        <div className='form-group' >
                            <label htmlFor='asunto'>Asunto</label>
                            <input type='text' className='form-control' name='asunto'
                                   ref={this.asuntoInput}
                            />
                        </div>
                        <div className='form-group' >
                            <label htmlFor='mensaje'>Mensaje</label>
                            <textarea className='form-control' name='mensaje'
                                      ref={this.mensajeInput}
                            ></textarea>
                        </div>
                        <button className="btn btn-primary my-2" type="submit" >
                            Enviar
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}
export default FormUncontrolled;
*/
