import React from "react";
import {configure, shallow} from "enzyme";
import chai, {expect} from "chai";
import chaiEnzyme from "chai-enzyme";
import Adapter from "enzyme-adapter-react-16";
import App from "../App";

import { act } from "react-dom/test-utils";

import configureMockStore from 'redux-mock-store'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk';

import * as actions from '../actions/CounterActions';
import * as types from "../actions/ActionTypes";
import reducer from '../reducers/counterReducer';
import { createStore } from 'redux';

const mockStore = configureMockStore([thunk]);


configure({
    adapter: new Adapter()
});

let wrapper;
beforeEach(() => {
    const store = mockStore({
        count: 0,
    });
    act(() => {
        wrapper = shallow(
            <Provider store={store}>
                <App />
            </Provider>
        );
    });

});
afterEach(() => {
    wrapper.unmount();
});


describe("Testing Actions", () => {
    it("Increment action must add one", () => {
        const action = {
            type: types.INCREMENT
        }
        expect(actions.increment()).to.eql(action);
    });
    chai.use(chaiEnzyme());
});


describe("Testing Reducers", () => {
    let store
    store = createStore(reducer);

    it("Test initial state", () => {
        expect(store.getState().count).to.equal(0);
    });
    it("Add one to the initial state", () => {
        store.dispatch({ type: types.INCREMENT });
        expect(store.getState().count).to.equal(1);
    });
    it("Remove one to the initial state", () => {
        store.dispatch({ type: types.DECREMENT });
        expect(store.getState().count).to.equal(0);
    });
    chai.use(chaiEnzyme());
});

