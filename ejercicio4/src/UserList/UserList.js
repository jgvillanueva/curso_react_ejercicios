import UserDetail from "../UserDetail/UserDetail";
import React, { useEffect, useState } from "react";

const UserList = (props) => {
    const [userData, setUserData] = useState(null);
    const [users, setUsers] = useState([]);
    const [error, setError] = useState(null);

    const getData = async () => {
        const url = 'http://dev.contanimacion.com/api_tablon/api/users';
        const response = await fetch(url, {
            method: 'GET',
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    setUsers(result);
                },
                (error) => {
                    setError(error.message);
                }
            )
    }

    const selectUser = (data) => {
        setUserData(data);
    }

    useEffect(() => {
        getData();
    }, [])

    if(error) {
        return(
            <div>Error cargando los datos {error}</div>
        )
    } else {
        return (
            <div className="userList">
                <ul>
                    {users.map(user => (
                        <li key={user.id}>
                            <span>{user.name}</span>
                            <button onClick={() => {selectUser(user)}}>Seleccionar</button>
                        </li>
                    ))}
                </ul>
                <UserDetail userData={userData}/>
            </div>
        )
    }

}

export default UserList;
