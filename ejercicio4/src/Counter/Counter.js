import React, { useState } from 'react';

function Counter() {
    const [counter, setCounter] = useState(0);

    const addOne = () => {
        setCounter(counter + 1);
    }

    return(
        <div className="counter">
            <h1>{counter}</h1>
            <button onClick={addOne}>Añade al contador</button>
        </div>
    );
}
export default Counter;
