import React, { useEffect, useState } from "react";

const UserDetail = (props) => {
    const {userData} = props;
    const [anteriores, setAnteriores] = useState([]);

    useEffect(() => {
        console.log('Cargado user', userData);
        const _anteriores = anteriores;
        _anteriores.push(userData.name);
        setAnteriores(_anteriores);
    }, [userData])

    if(!userData) {
        return (
            <div>Selecciona un usuario</div>
        )
    }
    return (
        <div>
            <div>
                <strong>Id</strong>
                <span>{userData.id}</span>
            </div>
            <div>
                <strong>Name</strong>
                <span>{userData.name}</span>
            </div>
            <div>
                Anteriores {
                anteriores.join(', ')
            }
            </div>
        </div>
    )
}

export default UserDetail;
