import logo from './logo.svg';
import './App.css';
import Counter from "./Counter/Counter";
import UserList from "./UserList/UserList";
import React, { useEffect, useState } from "react";

function App() {

  return (
    <div className="App">
      <header className="App-header">
        <UserList/>
        <Counter/>
      </header>
    </div>
  );
}

export default App;
