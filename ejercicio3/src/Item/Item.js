import React, { Component } from 'react';
import './item.css';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = { ampliado : false };
        //this.changeAmpliado = this.changeAmpliado.bind(this);
    }

    changeAmpliado() {
        this.setState(state => ({
            ampliado: !state.ampliado
        }));
    }
    render() {
        /*
        let contenido_ampliado;
        if(this.state.ampliado){
            contenido_ampliado = (<div><h2>{this.props.data.edad}</h2></div>);
        }
        return (
            <li className="item" onClick={this.changeAmpliado.bind(this)} >
                {this.props.nombre} x {this.props.children}
                {contenido_ampliado}
            </li>
        );
         */
        /*
        return (
            <li className="item" onClick={this.changeAmpliado.bind(this)} >
                {this.props.nombre} x {this.props.children}
                {
                    this.state.ampliado &&
                        <div><h2>{this.props.data.edad}</h2></div>
                }
            </li>
        );
         */
        return (
            <li className="item" onClick={this.changeAmpliado.bind(this)} >
                {this.props.data.nombre} x {this.props.children}
                {
                    this.state.ampliado ? <div><h2>{this.props.data.edad}</h2></div> : null
                }
            </li>
        );
    }
}

export default Item;

