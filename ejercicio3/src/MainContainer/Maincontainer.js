import React, { Component } from 'react';
import './mainContainer.css';
import List from '../List/List';
import Warning from "../warning/Warning";

class Maincontainer extends Component {
    elementos = [
        {id:1, nombre:"Jorge", edad: 45},
        {id:2, nombre:"Juan", edad: 37},
    ]

    constructor(props) {
        super(props);
        this.state = {
            showWarning: false,
        }

        this.toggleWarning = this.toggleWarning.bind(this);
    }

    toggleWarning() {
        const showWarning = !this.state.showWarning;
        this.setState({
            showWarning: showWarning,
        })
    }

    render() {
        return (
            <div className="list">
                <List data={this.elementos}></List>

                {
                    this.state.showWarning ?
                        <Warning clickHandler={this.toggleWarning}></Warning>
                        :
                        <button onClick={this.toggleWarning}>Abrir alerta</button>
                }
            </div>
        )
    }
}

export default Maincontainer;
