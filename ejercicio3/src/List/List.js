import React, { Component } from 'react';
import './list.css';
import Item from '../Item/Item';

class List extends Component {

    render() {
        var items = this.props.data.map((item, i) => {
            return <Item data={item} key={i}></Item>
        });
        return (
            <ul className="List">
                {items}
            </ul>
        );
    }
}

export default List;
