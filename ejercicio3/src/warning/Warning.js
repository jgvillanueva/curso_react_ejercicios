import React, { Component } from 'react';
import './warning.css';

class Warning extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="warning">
                <h1>Alerta!</h1>
                <button onClick={this.props.clickHandler}>Aceptar</button>
            </div>
        )
    }
}

export default Warning;
