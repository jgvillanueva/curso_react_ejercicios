import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import Maincontainer from "./MainContainer/Maincontainer";

class App extends Component {

  render() {
    return (
      <div className="app">
        <Maincontainer/>
      </div>
    );
  }
}

export default App;
