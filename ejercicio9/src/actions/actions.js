import * as types from './actionTypes';

export function addTodo (toDo){
    return{
        type: types.ADD_TODO,
        value: toDo
    }

}
export function removeTodo (id){
    return{
        type: types.REMOVE_TODO,
        value: id
    }

}
