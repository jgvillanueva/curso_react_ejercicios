import React from "react";
import { addTodo } from '../actions/actions';
import {connect} from 'react-redux';

class New extends React.Component {
    handleSubmit(event) {
        event.preventDefault();
        const {addTodo, history} = this.props;
        addTodo({
            toDo: {
                name: event.target.name.value,
                description: event.target.description.value,
            }
        })

        history.push('/list');
    }

    render(){
        return(
            <div className="row justify-content-md-center">
                <div className="new card m-3 col-6 p-3">
                    <h1 className="card-header">New</h1>
                    <form className="p-3" onSubmit={this.handleSubmit.bind(this)}>
                        <div className='form-group' >
                            <label htmlFor='name'>Name</label>
                            <input
                                type='text'
                                className='form-control'
                                name='name'
                            />
                        </div>
                        <div className='form-group' >
                            <label htmlFor='description'>Description</label>
                            <textarea
                                className='form-control'
                                name='description'
                            ></textarea>
                        </div>
                        <button className="btn btn-primary">Add ToDo</button>
                    </form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = (dispatch) => {
    return {
        addTodo: (toDo) => {
            dispatch(addTodo(toDo));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(New);
