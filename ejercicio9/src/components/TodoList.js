import React from "react";
import {connect} from 'react-redux';
import TodoItem from "./TodoItem";

class TodoList extends React.Component {
    render(){
        const {todoList} = this.props;
        const list = todoList.map((todo) =>
            <TodoItem
                key={todo.id}
                name={todo.name}
                todoId={todo.id}
                description={todo.description}/>
        )

        return(
            <div className="list card m-3">
                <h1 className="card-header">ToDos</h1>
                <div className="card-body">
                    <ul>
                        {list}
                    </ul>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    todoList: state.todoReducers.todoList,
})
const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
