

import React from "react";
import {removeTodo} from "../actions/actions";
import {connect} from "react-redux";

class TodoItem extends React.Component {
    constructor(props){
        super(props);
    }

    handleClick() {
        const {removeTodo, todoId} = this.props;
        removeTodo({
            id: todoId
        })
    }

    render(){
        return(
            <div className="item card card-body m-3 col-3">
                <h4 className="header">{this.props.name}</h4>
                <p>{this.props.description}</p>
                <button
                    className="btn btn-primary"
                    onClick={this.handleClick.bind(this)}
                >Remove</button>
            </div>
        )
    }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = (dispatch) => {
    return {
        removeTodo: (id) => {
            dispatch(removeTodo(id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoItem);
