import * as types from '../actions/actionTypes';

const initialState = {
    todoList: [
        {
            name: 'Montar los reducers',
            description: 'En todoreducer hay que añadir los métodos que ejecutan las acciones',
            id: 0,
        }
    ]
}

export default function reducer(state=initialState, action={}) {
    switch(action.type) {
        case types.ADD_TODO:
            const {toDo} = action.value;
            toDo.id = state.todoList.length;
            state.todoList.push(toDo);
            return{
                ...state,
                todoList: state.todoList.slice(),
            }
        case types.REMOVE_TODO:
            const {id} = action.value;
            state.todoList.forEach((toDo, index) => {
                if(toDo.id === id) {
                    state.todoList.splice(index, 1);
                }
            })
            return{
                ...state,
                todoList: state.todoList.slice(),
            }
        default:
            return{
                ...state,
                ...action,
            }
    }
}
