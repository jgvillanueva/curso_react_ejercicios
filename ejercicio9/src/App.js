import './App.css';
import {connect} from 'react-redux';
import {
  BrowserRouter as Router,
  Link,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import TodoList from "./components/TodoList";
import New from "./components/New";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <Router>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link to='/list' className="nav-link active">ToDos</Link>
            </li>
            <li className="nav-item">
              <Link to='/new' className="nav-link active">New</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/" exact>
            <Redirect to='/list' />
          </Route>
          <Route path="/list" component={TodoList}/>
          <Route path="/new" component={New}/>
        </Switch>
      </Router>
    </div>
  );
}

export default connect()(App);
