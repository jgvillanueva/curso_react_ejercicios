import React, { Component } from 'react';


class Detail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mensaje: null
        }

        this.getMensaje = this.getMensaje.bind(this);
    }

    async componentDidMount() {

        const location  = this.props.location;
        console.log(location);
        if(!location.state) {
            // con id
            const id = this.props.match.params.id;
            const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes/get/' + id;
            const response = await fetch(url, {
                method: 'GET',
            })
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            mensaje: result
                        });
                    }
                )
        } else {
            // con objeto
            console.log(location);
            const { data } = this.props.location.state;
            this.setState({
                mensaje: data
            });
        }

    }

    getMensaje() {
        const mensaje = this.state.mensaje;
        if (mensaje) {
            return (
                <div>
                    <h1>{mensaje.asunto}</h1>
                    <h4>Usuario {mensaje.user_id}</h4>
                    <p>{mensaje.created_at}</p>
                    <p>{mensaje.mensaje}</p>
                </div>
            )
        }
    }

    render() {
        return (
            <div>
                <h2>Vista de detalle</h2>
                {
                    this.getMensaje()
                }
            </div>

        )
    }
}
export default Detail;
