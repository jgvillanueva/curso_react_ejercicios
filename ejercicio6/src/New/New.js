import React, { Component } from 'react';
import Toggle from "../Toogle/Toggle";
import FormControlled from "../FormControlled/FormControlled";
import FormUncontrolled from "../FormUncontrolled/FormUncontrolled";

class New extends Component {
    constructor(props) {
        super(props);
        this.state = {
            controlled: true
        };
    }

    toggleChange(value) {
        this.setState({
            controlled: value
        })
    }

    render() {
        const controlled = this.state.controlled;
        return (
            <div className="new">
                <Toggle
                    label="Tipo de formulario"
                    option1="Controlled"
                    option2="Uncontrolled"
                    toggle={controlled}
                    handleChange={this.toggleChange.bind(this)}
                />
                {
                    controlled ?
                        <FormControlled/>
                        :
                        <FormUncontrolled/>
                }
            </div>
        )
    }
}
export default New;
