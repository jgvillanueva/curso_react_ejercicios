import React, { Component } from 'react';
import {
    BrowserRouter as Router,
        Switch,
        Route,
        Link,
    Redirect
} from "react-router-dom";
import List from "../List/List";
import New from "../New/New";
import './Container.scss';
import ListContainer from "../ListContainer/ListContainer";

class Container extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <div className="container">
                    <nav>
                        <ul>
                            <li>
                                <Link to="/">Home</Link>
                            </li>
                            <li>
                                <Link to="/new">New</Link>
                            </li>
                        </ul>
                    </nav>

                    <Switch>
                        <Route path="/" exact>
                            <Redirect to="/list" />
                        </Route>
                        <Route path="/list" >
                            <ListContainer />
                        </Route>
                        <Route path="/new">
                            <New />
                        </Route>
                    </Switch>
                </div>
            </Router>

        )
    }
}
export default Container;
