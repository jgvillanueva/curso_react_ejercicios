import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';

class Toggle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toggle: this.props.toggle
        };
    }

    handleClick() {
        const toggle = !this.state.toggle;
        this.setState({
            toggle
        })
        this.props.handleChange(toggle);
    }

    render() {
        const toggle = this.state.toggle;
        return (
            <div className="m-3">
                <span>{this.props.label}:</span>
                <button className="btn btn-primary mx-2" onClick={this.handleClick.bind(this)}>{
                    toggle ? <span>{this.props.option1}</span> : <span>{this.props.option2}</span>
                }</button>
            </div>
        )
    }
}
export default Toggle;
