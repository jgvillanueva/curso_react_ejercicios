import React, { Component } from 'react';
import Error from "../Error/Error";
class FormControlled extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mensaje: {
                asunto: '',
                mensaje: '',
                user_id: '13',
            },
            formErrors: {
                asunto: 'Necesitamos un asunto',
                mensaje: 'Hay que poner mensaje'
            },
            formTouched: {
                asunto: false,
                mensaje: false
            },
            formValid: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }

    handleChange(event) {
        const mensaje = this.state.mensaje;

        mensaje[event.target.name] = event.target.value;
        /*this.setState({
            data,
        });*/
        this.setState(
            {mensaje},
            () => { this.validateField(event.target.name, event.target.value) }
            );
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log('Datos enviados', this.state.mensaje);
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let valid;
        let message;
        switch(fieldName) {
            case 'asunto':
                valid = value.length >= 5;
                message = valid ? '' : ' es muy corto (5 caracteres)';
                break;
            case 'mensaje':
                valid = value.length >= 15;
                message = valid ? '': ' es muy corto (15 caracteres)';
                break;
            default:
                break;
        }
        fieldValidationErrors[fieldName] = message;
        this.setState({formErrors: fieldValidationErrors}, this.validateForm);
    }

    validateForm() {
        const formErrors = this.state.formErrors;
        let formValid = true;
        Object.keys(formErrors).forEach((field) => {
            if (formErrors[field] !== '') {
                formValid = false;
            }
        });
        this.setState({
            formValid
        });
    }

    handleBlur(event) {
        const formTouched = this.state.formTouched;
        console.log(event.target.name);
        formTouched[event.target.name] = true;
        console.log(formTouched);
        this.setState({
            formTouched
        });
    }

    render() {
        return (
            <div className='card'>
                <div className="card-header">
                    <h1>Controlled form</h1>
                </div>
                <div className="card-body">
                    <form className='formulario' onSubmit={this.handleSubmit}>
                        <div className='form-group' >
                            <label htmlFor='email'>Asunto</label>
                            <input
                                type='text'
                                className='form-control'
                                name='asunto'
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                                value={this.state.mensaje.asunto}
                            />
                            <Error
                                errorText={this.state.formErrors.asunto}
                                touched={this.state.formTouched.asunto}
                            />
                        </div>
                        <div className='form-group' >
                            <label htmlFor='mensaje'>Mensaje</label>
                            <textarea
                                className='form-control'
                                name='mensaje'
                                onChange={this.handleChange}
                                onBlur={this.handleBlur}
                                value={this.state.mensaje.mensaje}
                            ></textarea>
                            <Error
                                errorText={this.state.formErrors.mensaje}
                                touched={this.state.formTouched.mensaje}
                            />
                        </div>
                        <div className='form-group' >
                            <label htmlFor='user_id'>User id</label>
                            <select
                                className='form-control'
                                name='user_id'
                                onChange={this.handleChange}
                                value={this.state.mensaje.user_id}
                            >
                                <option value="11">Carmen</option>
                                <option value="12">Claudia</option>
                                <option value="13">Jorge</option>
                                <option value="14">David</option>
                            </select>
                        </div>
                        <button
                            className="btn btn-primary my-2"
                            type="submit"
                            disabled={!this.state.formValid}
                        >
                            Enviar
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}
export default FormControlled;
