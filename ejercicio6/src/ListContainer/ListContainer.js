import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import List from "../List/List";
import Detail from "../Detail/Detail";

class ListContainer extends Component {


    constructor(props) {
        super(props);

        this.state = {
        }

    }

    render() {
        return (
            <div>
                <h1>Lista de Mensajes</h1>
                <Router>
                    <Switch>
                        <Route exact path='/list/:id' component={Detail}>
                        </Route>
                        <Route exact path='/list'>
                            <List />
                        </Route>
                    </Switch>
                </Router>
            </div>
        )
    }
}
export default ListContainer;
