import React, { Component } from 'react';
import './List.scss';
import {Link, withRouter} from "react-router-dom";

class List extends Component {
    user_id = 13;

    constructor(props) {
        super(props);

        this.state = {
            mensajes: []
        }

        this.changeUrl = this.changeUrl.bind(this);
    }

    async componentDidMount() {
        const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
        const response = await fetch(url, {
            method: 'GET',
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        mensajes: result
                    });
                }
            )
    }

    getClassCard(mensaje) {
        return this.user_id === mensaje.user_id ? 'card propio': 'card';
    }

    changeUrl(id) {
        this.props.history.push('/list/' + id);
    }

    getItems() {
        return (
            this.state.mensajes.map((mensaje)=> {
                return (
                    <li key={mensaje.id} className={this.getClassCard(mensaje)}
                        >
                        {/*<Link to={'/list/' + mensaje.id}>*/}
                        <div className="card-header" onClick={() => {this.changeUrl(mensaje.id)}}
                        >{mensaje.asunto}</div>
                        <div className="card-body">{mensaje.mensaje}</div>
                        {/*</Link>*/}
                        <Link to={{
                            pathname: '/list/' + mensaje.id,
                            state: {
                                data: mensaje
                            }
                        }}>
                            <button className="btn btn-primary">Ver</button>
                        </Link>
                    </li>
                )
            })
        )
    }

    render() {
        return (
            <div className="list">
                <ul>
                    {this.getItems()}
                </ul>
            </div>
        )
    }
}
export default withRouter(List);
